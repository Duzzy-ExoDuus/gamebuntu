const { exec } = require("child_process")

_run_script = (script, pkexec='pkexec') => {
    exec(`x-terminal-emulator -e 'cd $(mktemp -d) && cp $(realpath ${__dirname}/../scripts/${script}) . && chmod 755 $(basename ${script}) && ${pkexec} $(realpath $(basename ${script})); echo; echo "Finished. Press ENTER or close this window to exit."; read ENTER'`)
}

_install = (name) => {
    switch (name) {
        case 'steam':
            _run_script('launchers/steam.sh')
            break
        case 'gamehub':
            _run_script('launchers/gamehub.sh', '')
            break
        case 'heroic':
            _run_script('launchers/heroic.sh')
            break
        case 'minigalaxy':
            _run_script('launchers/minigalaxy.sh')
            break
        case 'playonlinux':
            _run_script('launchers/playonlinux.sh')
            break
        case 'lutris':
            _run_script('launchers/lutris.sh')
            break
        case 'retroarch':
            _run_script('launchers/retroarch.sh')
            break
        case 'yabause':
            _run_script('launchers/yabause.sh')
            break
        case 'stella':
            _run_script('launchers/stella.sh')
            break
        case 'obs':
            _run_script('streaming/obs.sh')
            break
        case 'mangohud':
            _run_script('tools/mangohud.sh')
            break
        case 'goverlay':
            _run_script('tools/goverlay.sh')
            break
        case 'gamemode':
            _run_script('tools/gamemode.sh')
            break
        case 'openrgb':
            _run_script('tools/openrgb.sh')
            break
        case 'wine':
            _run_script('tools/wine.sh')
            break
        case 'piper':
            _run_script('tools/piper.sh')
            break
        case 'polychromatic':
            _run_script('tools/polychromatic.sh')
            break
        case 'noisetorch':
            _run_script('tools/noisetorch.sh', '')
            break
        case 'vlc':
            _run_script('tools/vlc.sh')
            break
        case 'protonup-qt':
            _run_script('tools/protonup-qt.sh')
            break
        case 'vkbasalt':
            _run_script('tools/vkbasalt.sh')
            break
        case 'dosbox':
            _run_script('tools/dosbox.sh')
            break
        case 'lowlatency':
            _run_script('kernels/lowlatency.sh')
            break
        case 'xanmod':
            _run_script('kernels/xanmod.sh')
            break
        case 'discord':
            _run_script('social/discord.sh')
            break
        case 'mumble':
            _run_script('social/mumble.sh')
            break
    }
}